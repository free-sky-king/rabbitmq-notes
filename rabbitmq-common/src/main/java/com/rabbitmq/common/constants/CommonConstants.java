package com.rabbitmq.common.constants;

import java.time.format.DateTimeFormatter;

/**
 * @author
 * @Describe 功能描述
 * @date
 */
public class CommonConstants {
    /**
     * 时间格式
     */
    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss SSS");

    /***************rabbitMq常量**************/
    public static final String EXCHANGE_NAME_PREFIX = "kinson_exchange_";

    public static final String SPRING_EXCHANGE_NAME_PREFIX = "spring_kinson_exchange_";

    public static String[] ROUTING_KEYS = new String[]{"kinson_routingkey_critival", "kinson_routingkey_warn", "kinson_routingkey_info"};

    public static String[] TOPIC_ROUTING_KEYS = new String[]{"kinson.topic.critival", "kinson.topic.warn", "kinson.topic.info"};

    public static final String QUEUE_NAME_PREFIX = "kinson_queue_";

    public static final String SPRING_QUEUE_NAME_PREFIX = "spring_kinson_queue_";

    public static final String UTF_8 = "UTF-8";

    public static final String DLX_QUEUE_NAME_PREFIX = "dlx.";

    public static final String DELAYED_QUEUE_NAME_PREFIX = "delayed.";

}
