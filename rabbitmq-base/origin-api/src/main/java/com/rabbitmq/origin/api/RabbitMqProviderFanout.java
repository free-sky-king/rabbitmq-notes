package com.rabbitmq.origin.api;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.common.utils.RabbitMqUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author
 * @Describe 广播模式交换机生产消息, 该类型是发布订阅模式，生产者发布的一条消息会被所有订阅的队列所消费
 * @date
 */
@Slf4j(topic = "RabbitMqProviderFanout")
public class RabbitMqProviderFanout {

    /**
     * 发送广播模式消息,该模式不需要路由key,所有与交换机绑定的队列都能存放消息，声明队列的消费者都能消费消息
     * 生产者发送的一条消息会被N个消费者消费
     *
     * @param exchangeName
     */
    public void publishFanout(String exchangeName, String msg) {
        Connection conn = null;
        Channel channel = null;
        try {
            // 获取连接
            conn = RabbitMqUtil.getConn();
            // 创建通道（通道可用于消息的发送和接收）
            channel = conn.createChannel();
            // 声明交换机(持久的，非自动交换的默认为"direct"类型的Exchange),此处声明为fanout类型
            // 参数1交换机名称，参数2交换机类型，参数3是否持久化(false在mq服务重启后将会被删除))
            channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT, true);

            // 发布消息,广播模式不需要路由key
            channel.basicPublish(exchangeName, "", null, msg.getBytes());
        } catch (Exception e) {
            log.error("发布消息异常,", e);
        } finally {
            // log.info("[RabbitMqProviderFanout] publishFanout 关闭资源连接");
            RabbitMqUtil.close(conn, channel);
        }
    }
}
