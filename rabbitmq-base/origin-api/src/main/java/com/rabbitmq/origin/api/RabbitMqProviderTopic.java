package com.rabbitmq.origin.api;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.common.utils.RabbitMqUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author
 * @Describe 主题类型交换机, 该模式为消息主题模式。在直连类型的基础上，主题类型支持通配符路由匹配,通配符如下：
 * * 匹配不多不少恰好1个词
 * # 匹配零个、一个或多个词
 * 如: audit.#,匹配audit、audit.irs 、或者audit.irs.corporate等,audit.*只能匹配 audit.irs
 * @date
 */
@Slf4j(topic = "RabbitMqProviderTopic")
public class RabbitMqProviderTopic {

    /**
     * 主题方式，支持通配符路由匹配
     */
    public void publishTopic(String exchangeName, String routingKey, String msg) {
        Connection conn = null;
        Channel channel = null;
        try {
            // 获取连接
            conn = RabbitMqUtil.getConn();
            // 创建通道（通道可用于消息的发送和接收）
            channel = conn.createChannel();

            // 主题方式的交换机
            channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, true);

            // 发布消息,消息发送到完全匹配或通配的由的队列
            channel.basicPublish(exchangeName, routingKey, null, msg.getBytes());
        } catch (Exception e) {
            log.error("发布消息异常,", e);
        } finally {
            // log.info("[RabbitMqProviderTopic] publishTopic 关闭资源连接");
            RabbitMqUtil.close(conn, channel);
        }
    }
}
