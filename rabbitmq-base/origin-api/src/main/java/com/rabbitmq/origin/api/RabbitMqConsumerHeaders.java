package com.rabbitmq.origin.api;

import com.rabbitmq.client.*;
import com.rabbitmq.common.utils.RabbitMqUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Map;

/**
 * @author
 * @Describe 消费headers类型消息。
 * @date
 */
@Slf4j(topic = "RabbitMqConsumerHeaders")
public class RabbitMqConsumerHeaders {

    /**
     * 消费匹配headers的消息
     *
     * @param exchangeName
     */
    public void consumerHeaders(String exchangeName, int flag, Map<String, Object> headers) {
        Connection conn = null;
        Channel channel = null;
        try {
            // 获取连接
            conn = RabbitMqUtil.getConn();
            // 创建通道（通道可用于消息的发送和接收）
            channel = conn.createChannel();

            // 声明headers类型的交换机
            channel.exchangeDeclare(exchangeName, BuiltinExchangeType.HEADERS, true);

            // 声明临时队列，非持久，独占，自动删除队列(客户端停止，对应的临时队列会自动删除)，每次都是一个新队列
            String queueName = channel.queueDeclare().getQueue();

            // 绑定队列,该模式无需路由key,加上匹配的头部参数，队列绑定的参数如果为发送时的头部信息的子集时就可以匹配
            channel.queueBind(queueName, exchangeName, "", headers);

            // 创建队列消费者
            final Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                           byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");
                    if (flag % 3 == 1) {
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            log.error("Thread.sleep 异常", e);
                        }
                    }
                    log.info(" consumer-" + flag + " handleDelivery " + envelope.getRoutingKey() + ":'" + message + "'");
                }
            };

            // 消息消费,自动确认true,自定义标签headersConsumer
            channel.basicConsume(queueName, true, "headersConsumer", consumer);
        } catch (Exception e) {
            log.error("消费消息异常,", e);
            // 资源关闭
            RabbitMqUtil.close(conn, channel);
        }
    }
}
