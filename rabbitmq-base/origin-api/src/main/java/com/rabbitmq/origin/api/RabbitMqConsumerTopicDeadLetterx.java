package com.rabbitmq.origin.api;

import com.rabbitmq.client.*;
import com.rabbitmq.common.utils.RabbitMqUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @Describe 使用死信队列实现延迟队列。
 * 设置队列的ttl时间或者单个消息的过期时间，在普通的队列中超过设置的过世时间后会被死信交换机发送到死信队列，所以只要消费死信队列的消息即可实现延迟队列效果
 * @date
 */
@Slf4j(topic = "RabbitMqConsumerTopicDeadLetterx")
public class RabbitMqConsumerTopicDeadLetterx {

    /**
     * 队列设置ttl过期时间
     *
     * @param exchangeName
     */
    public void consumerTopicQueueDeadLetterx(String exchangeName, int flag, String... routingKeys) {
        Connection conn = null;
        Channel channel = null;
        try {
            // 获取连接
            conn = RabbitMqUtil.getConn();
            // 创建通道（通道可用于消息的发送和接收）
            channel = conn.createChannel();

            // 声明主题类型的交换机
            channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, true);

            // 声明临时队列，非持久，独占，自动删除队列(客户端停止，对应的临时队列会自动删除)，每次都是一个新队列
            String queueName = channel.queueDeclare().getQueue();

            // 绑定队列,该模式绑定的路由key支持和生产者交换机发送的路由key进行通配符匹配
            if (ArrayUtils.isEmpty(routingKeys)) {
                // 此处routingkey设置为#百事匹配所有的发送的路由key
                channel.queueBind(queueName, exchangeName, "#");
            } else {
                // 一个队列绑定多个路由key
                for (int i = 0; i < routingKeys.length; i++) {
                    channel.queueBind(queueName, exchangeName, routingKeys[i]);
                }
            }

            // 创建队列消费者
            final Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                           byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");
                    if (flag % 3 == 1) {
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            log.error("Thread.sleep 异常", e);
                        }
                    }
                    log.info(" consumer-" + flag + " handleDelivery " + envelope.getRoutingKey() + ":'" + message + "'");
                }
            };

            // 消息消费，自动确认
            channel.basicConsume(queueName, true, "topicConsumer", consumer);
        } catch (Exception e) {
            log.error("消费消息异常,", e);
            // 资源关闭
            RabbitMqUtil.close(conn, channel);
        }
    }

    /**
     * 单个消息设置过期时间
     * 使用在消息属性上设置TTL的方式(同一个队列的消息)，消息可能并不会按时变成死信消息，因为RabbitMQ只会检查第一个消息是否过期，如果过期则丢到死信队列，
     * 如果第一个消息的延时时长很长，而第二个消息的延时时长很短，则第二个消息并不会优先得到执行，这样即使短时间的过期时间到了也不会被丢弃。
     *
     * @param exchangeName
     */
    public void consumerTopicSingleMsgTtlDeadLetterx(String exchangeName, String routingKey, String queueName) {
        Connection conn = null;
        Channel channel = null;
        try {
            // 获取连接
            conn = RabbitMqUtil.getConn();
            // 创建通道（通道可用于消息的发送和接收）
            channel = conn.createChannel();

            // 声明主题类型的交换机
            channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, false, true, null);

            // 声明队列
            // 死信队列参数
            Map<String, Object> dlxArguments = new HashMap<String, Object>();
            // 为队列设置x-queue-mode参数,将队列设置为延迟模式,在磁盘上保留尽可能多的消息,以减少RAM的使用;
            // 如果未设置，队列将保留内存缓存以尽可能快地传递消息;默认值: false.
            dlxArguments.put("x-queue-mode", "lazy");
            channel.queueDeclare(queueName, true, false, false, dlxArguments);

            // 绑定队列,该模式绑定的路由key支持和生产者交换机发送的路由key进行通配符匹配
            channel.queueBind(queueName, exchangeName, routingKey);

            // 创建队列消费者
            final Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                           byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");
                    log.info(" consumerTopicSingleMsgDeadLetterx handleDelivery " + envelope.getRoutingKey() + ":'" + message + "'");
                }
            };

            // 消息消费，自动确认
            channel.basicConsume(queueName, true, "consumerTopicSingleMsgDeadLetterx", consumer);
        } catch (Exception e) {
            log.error("消费消息异常,", e);
            // 资源关闭
            RabbitMqUtil.close(conn, channel);
        }
    }
}
