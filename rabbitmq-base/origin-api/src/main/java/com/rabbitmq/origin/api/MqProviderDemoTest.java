package com.rabbitmq.origin.api;

import com.rabbitmq.client.BuiltinExchangeType;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

import static com.rabbitmq.common.constants.CommonConstants.*;

/**
 * @author
 * @Describe 功能描述
 * @date
 */
@Slf4j(topic = "MqProviderDemoTest")
public class MqProviderDemoTest {

    public static void main(String[] args) {


        String date = LocalDateTime.now().format(DATETIME_FORMATTER);

        /***************1. 默认交换机消息发送,生产者和消费者直接连接队列进行消息发送和消费***************/
        /*RabbitMqProviderQueue rabbitMqProviderQueue = new RabbitMqProviderQueue();
        String methodName = "publishQueue";
        for (int i = 1; i <= 9; i++) {
            String curMsg = " method " + methodName + "第" + i + "次发送消息";
            rabbitMqProviderQueue.publishQueue(QUEUE_NAME_PREFIX + methodName, curMsg);
            log.info(curMsg);
        }*/

        /***************2. 指定交换机消息发送***************/
        // 2.1 交换机类型为fanout
        /*RabbitMqProviderFanout rabbitMqProviderFanout = new RabbitMqProviderFanout();
        String exchangeType = BuiltinExchangeType.FANOUT.getType();
        String methodName = "publishFanout";
        for (int i = 1; i <= 3; i++) {
            String curMsg = exchangeType + " method " + methodName + "第" + i + "次发送消息";
            rabbitMqProviderFanout.publishFanout(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName, curMsg);
            System.out.println(curMsg);
        }*/

        // 2.2 交换机类型为direct
        /*RabbitMqProviderRouting rabbitMqProviderRouting = new RabbitMqProviderRouting();
        String exchangeType = BuiltinExchangeType.DIRECT.getType();
        String methodName = "publishDirect";
        for (int i = 1; i <= 9; i++) {
            String routingKey = ROUTING_KEYS[(i-1) % 3];
            String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息,cur routingKey is " + routingKey;
            rabbitMqProviderRouting.publishDirect(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                    routingKey, curMsg);
            System.out.println(curMsg);
        }*/

        // 2.3 交换机类型为topic
        /*RabbitMqProviderTopic rabbitMqProviderTopic = new RabbitMqProviderTopic();
        String exchangeType = BuiltinExchangeType.TOPIC.getType();
        String methodName = "publishTopic";
        for (int i = 1; i <= 9; i++) {
            String routingKey = TOPIC_ROUTING_KEYS[(i-1) % 3];
            String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息,cur routingKey is " + routingKey;
            rabbitMqProviderTopic.publishTopic(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                    routingKey, curMsg);
            System.out.println(curMsg);
        }*/

        // 2.4 交换机类型为headers
        /*RabbitMqProviderHeaders rabbitMqProviderHeaders = new RabbitMqProviderHeaders();
        // 构建headers
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("param1", "param1");
        headers.put("param2", "param2");
        headers.put("param3", "param3");
        headers.put("param4", "param4");
        headers.put("param5", "param5");

        String exchangeType = BuiltinExchangeType.HEADERS.getType();
        String methodName = "publishHeaders";
        for (int i = 1; i <= 9; i++) {
            String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息";
            rabbitMqProviderHeaders.publishHeaders(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,  headers, curMsg);
            System.out.println(curMsg);
        }*/

        /*******************3.消费端使用指定队列绑定交换机发送消息及消费消息*******************/
        // 3.1 交换机类型为fanout
        /*RabbitMqProviderExchange2Queue rabbitMqProviderExchange2Queue = new RabbitMqProviderExchange2Queue();
        String exchangeType = BuiltinExchangeType.FANOUT.getType();
        String methodName = "publishExchange2Queue";
        for (int i = 1; i <= 3; i++) {
            String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息";
            rabbitMqProviderExchange2Queue.publishExchange2Queue(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                    exchangeType, "", curMsg);
            System.out.println(curMsg);
        }*/

        // 3.2 交换机类型为direct
        /*RabbitMqProviderExchange2Queue rabbitMqProviderExchange2Queue = new RabbitMqProviderExchange2Queue();
        String exchangeType = BuiltinExchangeType.DIRECT.getType();
        String methodName = "publishExchange2Queue";
        for (int i = 1; i <= 9; i++) {
            String routingKey = ROUTING_KEYS[(i-1) % 3];
            String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息,cur routingKey is " + routingKey;
            rabbitMqProviderExchange2Queue.publishExchange2Queue(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                    exchangeType, routingKey, curMsg);
            System.out.println(curMsg);
        }*/

        // 3.3 交换机类型为topic
        /*RabbitMqProviderExchange2Queue rabbitMqProviderExchange2Queue = new RabbitMqProviderExchange2Queue();
        String exchangeType = BuiltinExchangeType.TOPIC.getType();
        String methodName = "publishExchange2Queue";
        for (int i = 1; i <= 9; i++) {
            String routingKey = TOPIC_ROUTING_KEYS[(i-1) % 3];
            String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息,cur routingKey is " + routingKey;
            rabbitMqProviderExchange2Queue.publishExchange2Queue(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                    exchangeType, routingKey, curMsg);
            System.out.println(curMsg);
        }*/

        // 3.4 交换机类型为topic,设置了死信队列
        // 3.4.1设置队列的ttl（Time To Live）时间
        /*RabbitMqProviderTopicDeadLetterx rabbitMqProviderTopicDeadLetterx = new RabbitMqProviderTopicDeadLetterx();
        String exchangeType = BuiltinExchangeType.TOPIC.getType();
        String methodName = "publishTopicQueueTtlDeadLetterx";
        for (int i = 1; i <= 3; i++) {
            String routingKey = TOPIC_ROUTING_KEYS[(i-1) % 3];
            String queueName = QUEUE_NAME_PREFIX + exchangeType + "_" + methodName + i;
            String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息,cur routingKey is " +
                    routingKey + ",cur queueName is " + queueName;
            rabbitMqProviderTopicDeadLetterx.publishTopicQueueTtlDeadLetterx(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                    routingKey, queueName, curMsg);
            System.out.println(curMsg);
        }*/

        // 3.4.2设置单个消息的过期时间(不同的队列)
        /*RabbitMqProviderTopicDeadLetterx rabbitMqProviderTopicDeadLetterx = new RabbitMqProviderTopicDeadLetterx();
        String exchangeType = BuiltinExchangeType.TOPIC.getType();
        String methodName = "publishTopicSingleMsgTtlDeadLetterx";
        for (int i = 1; i <= 3; i++) {
            String routingKey = TOPIC_ROUTING_KEYS[(i-1) % 3];
            // 不同的队列
            String queueName = QUEUE_NAME_PREFIX + exchangeType + "_" + methodName + i;
            String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息,cur routingKey is " +
                    routingKey + ",cur queueName is " + queueName + ("(ttl=" + (4 - i) + "0000" + ")");
            rabbitMqProviderTopicDeadLetterx.publishTopicSingleMsgTtlDeadLetterx(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                    routingKey, queueName, curMsg + (4 - i) + "0000", (4 - i) + "0000");
            System.out.println(curMsg);
        }*/

        // 3.4.3设置单个消息的过期时间(同一个队列)
        RabbitMqProviderTopicDeadLetterx rabbitMqProviderTopicDeadLetterx = new RabbitMqProviderTopicDeadLetterx();
        String exchangeType = BuiltinExchangeType.TOPIC.getType();
        String methodName = "publishTopicSingleMsgTtlDeadLetterx";
        // 统一发送到第一个队列中
        int i = 1;
        String routingKey = TOPIC_ROUTING_KEYS[(i-1) % 3];
        String queueName = QUEUE_NAME_PREFIX + exchangeType + "_" + methodName + i;
        String curMsg = exchangeType + "-" + "method " + methodName + "第" + i + "次发送消息,cur routingKey is " +
                routingKey + ",cur queueName is " + queueName;
        // 第一个消息的过期时间长（或第一个过期时间短）
        String expiration = "5000";
        //expiration = "1000";
        rabbitMqProviderTopicDeadLetterx.publishTopicSingleMsgTtlDeadLetterx(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                routingKey, queueName, curMsg + expiration, expiration);
        System.out.println(curMsg + expiration);
        // 第二个消息的过期时间短（或第二个过期时间长）
        expiration = "1000";
        //expiration = "5000";
        rabbitMqProviderTopicDeadLetterx.publishTopicSingleMsgTtlDeadLetterx(EXCHANGE_NAME_PREFIX + exchangeType + "_" + methodName,
                routingKey, queueName, curMsg + expiration, expiration);
        System.out.println(curMsg + expiration);
    }
}

