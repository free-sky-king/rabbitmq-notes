package com.rabbitmq.origin.api;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import com.rabbitmq.common.utils.RabbitMqUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author
 * @Describe 功能描述
 * @date
 */
@Slf4j(topic = "RabbitMqProviderExchange2Queue")
public class RabbitMqProviderExchange2Queue {

    public void publishExchange2Queue(String exchangeName, String exchangeType, String routingKey, String msg) {
        Connection conn = null;
        Channel channel = null;
        try {
            // 获取连接
            conn = RabbitMqUtil.getConn();
            // 创建通道（通道可用于消息的发送和接收）
            channel = conn.createChannel();

            // 声明交换机(持久的，非自动交换的默认为"direct"类型的Exchange)
            // 参数1交换机名称，参数2交换机类型，参数3是否持久化(false在mq服务重启后将会被删除))
            channel.exchangeDeclare(exchangeName, exchangeType, true);

            // 发布消息
            // channel.basicPublish(exchangeName, routingKey, null, msg.getBytes());
            // 发布消息，设置消息的属性为PERSISTENT_TEXT_PLAIN
            channel.basicPublish(exchangeName, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN, msg.getBytes());
        } catch (Exception e) {
            log.error("发布消息异常,", e);
        } finally {
            // log.info("[RabbitMqProviderExchange2Queue] publishExchange2Queue 关闭资源连接");
            RabbitMqUtil.close(conn, channel);
        }
    }
}
