package com.rabbitmq.origin.api;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.common.utils.RabbitMqUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author
 * @Describe 直连消息队列生产消息
 * @date
 */
@Slf4j(topic = "RabbitMqProviderQueue")
public class RabbitMqProviderQueue {

    /**
     * 直连队列方式：通过默认的交换机发布消息到队列，此时routingKey要和队列名称一样，否则发送的消息不会进队列
     */
    public void publishQueue(String queueName, String msg) {
        Connection conn = null;
        Channel channel = null;
        try {
            // 获取连接
            conn = RabbitMqUtil.getConn();
            // 创建通道（通道可用于消息的发送和接收）
            channel = conn.createChannel();

            // 队列声明
            channel.queueDeclare(queueName, true, false, false, null);

            // 发布消息,交换机为空时为默认的direct类型的交换机
            // 此时routingKey要和队列名一致，因为队列会默认和空字符串exchange进行绑定且routingKey为队列名
            // 如果routingKey和队列名不一样则消息无法发送到队列
            channel.basicPublish("", queueName, null, msg.getBytes());
        } catch (Exception e) {
            log.error("发布消息异常,", e);
        } finally {
            // log.info("[RabbitMqProviderQueue] publishQueue 关闭资源连接");
            RabbitMqUtil.close(conn, channel);
        }
    }
}
