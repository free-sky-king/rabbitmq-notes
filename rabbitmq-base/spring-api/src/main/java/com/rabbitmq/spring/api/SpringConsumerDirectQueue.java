package com.rabbitmq.spring.api;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

import static com.rabbitmq.common.constants.CommonConstants.*;

/**
 * @author
 * @Describe direct交换机，消费端使用指定的队列
 * @date
 */
@Component
@Slf4j(topic = "SpringConsumerDirectQueue")
public class SpringConsumerDirectQueue {

    /**
     * 在注解里进行队列绑定交换机和路由信息
     *
     * @param message
     * @throws UnsupportedEncodingException
     */
    @RabbitListener(bindings = @QueueBinding(value = @Queue(name = SPRING_QUEUE_NAME_PREFIX + "direct" + "_publishDirectQueue1"), // 声明队列
            exchange = @Exchange(name = SPRING_EXCHANGE_NAME_PREFIX + "direct" + "_publishDirectQueue", type = "direct"),
            key = {"kinson_routingkey_critival"} // 路由
    ))
    public void receive(String sendMsg, Channel channel, Message message) throws UnsupportedEncodingException {
        MessageProperties messageProperties = message.getMessageProperties();
        log.info(sendMsg + " ===== receive msg is {} ===== ", new String(message.getBody(), UTF_8) +
                ",headers is " + messageProperties.getHeaders());
    }

    @RabbitListener(bindings = @QueueBinding(value = @Queue(name = SPRING_QUEUE_NAME_PREFIX + "direct" + "_publishDirectQueue2"), // 声明队列
            exchange = @Exchange(name = SPRING_EXCHANGE_NAME_PREFIX + "direct" + "_publishDirectQueue", type = "direct"),
            key = {"kinson_routingkey_info"} // 路由
    ))
    public void receive2(String sendMsg, Channel channel, Message message) throws UnsupportedEncodingException {
        MessageProperties messageProperties = message.getMessageProperties();
        log.info(sendMsg + " ===== receive2 msg is {} ===== ", new String(message.getBody(), UTF_8) +
                ",headers is " + messageProperties.getHeaders());
    }

    /**
     * 手动声明队列bean
     *
     * @return
     */
    @Bean
    public org.springframework.amqp.core.Queue publishDirectQueueManual() {
        return new org.springframework.amqp.core.Queue(SPRING_QUEUE_NAME_PREFIX + "direct_publishDirectQueueManual");
    }

    /**
     * 手动声明交换机bean
     *
     * @return
     */
    @Bean
    public DirectExchange publishDirectExchangeManual() {
        return new DirectExchange(SPRING_EXCHANGE_NAME_PREFIX + "direct_publishDirectExchangeManual");
    }

    /**
     * 手动声明binding bean
     *
     * @return
     */
    @Bean
    public Binding bindQueue2ExchangeManual(org.springframework.amqp.core.Queue publishDirectQueueManual,
                                            DirectExchange publishDirectExchangeManual) {
        return BindingBuilder.bind(publishDirectQueueManual).to(publishDirectExchangeManual).with("kinson_routingkey_warn");
    }

    /**
     * 手动声明队列消费者（手动声明交换机和队列）,此处RabbitListener只需配置队列名
     *
     * @param message
     * @throws UnsupportedEncodingException
     */
    @RabbitListener(queues = SPRING_QUEUE_NAME_PREFIX + "direct_publishDirectQueueManual")
    public void receive3(Message message) throws UnsupportedEncodingException {
        MessageProperties messageProperties = message.getMessageProperties();
        log.info(" ===== receive3 msg is {} ===== ", new String(message.getBody(), UTF_8) +
                ",headers is " + messageProperties.getHeaders());
    }

}
