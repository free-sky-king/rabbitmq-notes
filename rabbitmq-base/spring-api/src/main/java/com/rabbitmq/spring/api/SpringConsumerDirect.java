package com.rabbitmq.spring.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static com.rabbitmq.common.constants.CommonConstants.SPRING_EXCHANGE_NAME_PREFIX;

/**
 * @author
 * @Describe direct交换机
 * @date
 */
@Component
@Slf4j(topic = "SpringConsumerDirect")
public class SpringConsumerDirect {

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, // 绑定临时队列
            exchange = @Exchange(name = SPRING_EXCHANGE_NAME_PREFIX + "direct" + "_publishDirect", type = "direct"), // 绑定交换机
            key = {"kinson_routingkey_critival", "kinson_routingkey_warn"} // 绑定的路由key
    ))
    void receive(String msg) {
        log.info(" ===== receive msg is {} ===== ", msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, // 临时队列
            exchange = @Exchange(name = SPRING_EXCHANGE_NAME_PREFIX + "direct" + "_publishDirect", type = "direct"),
            key = {"kinson_routingkey_warn"}
    ))
    void receive2(String msg) {
        log.info(" ===== receive2 msg is {} ===== ", msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, // 临时队列
            exchange = @Exchange(name = SPRING_EXCHANGE_NAME_PREFIX + "direct" + "_publishDirect", type = "direct"),
            key = {"kinson_routingkey_info"}
    ))
    void receive3(String msg) {
        log.info(" ===== receive3 msg is {} ===== ", msg);
    }
}
