package com.rabbitmq.spring.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

import static com.rabbitmq.common.constants.CommonConstants.*;


/**
 * @author
 * @Describe topic交换机，消费某个队列的死信消息实现延迟队列效果
 * @date
 */
@Component
@Slf4j(topic = "SpringConsumerTopicDeadLetterxDelay")
public class SpringConsumerTopicDeadLetterxDelay {

    /**
     * @param message
     * @throws UnsupportedEncodingException
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(
                    name = DLX_QUEUE_NAME_PREFIX + SPRING_QUEUE_NAME_PREFIX + "topic" + "_publishTopicQueueTtlDeadLetterx",
                    arguments = {
                            @Argument(name = "x-queue-mode", value = "lazy")
                    } // 队列参数绑定
            ), // 绑定队列
            exchange = @Exchange(name = DLX_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + "topic" + "_publishTopicQueueTtlDeadLetterx", type = "topic"), // 绑定交换机
            key = {DLX_QUEUE_NAME_PREFIX + "kinson.topic.critival"} // 绑定的路由key
    ))
    void consumeTopicQueueTtlDeadLetterx(Message message) throws UnsupportedEncodingException {
        log.info(" ===== consumeTopicQueueTtlDeadLetterx msg is {} ===== ", new String(message.getBody(), "UTF-8"));
    }

    /**
     * @param message
     * @throws UnsupportedEncodingException
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(
                    name = DLX_QUEUE_NAME_PREFIX + SPRING_QUEUE_NAME_PREFIX + "topic" + "_publishTopicSingleMsgTtlDeadLetterx",
                    arguments = {
                            @Argument(name = "x-queue-mode", value = "lazy")
                    } // 队列参数绑定
            ), // 绑定队列
            exchange = @Exchange(name = DLX_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + "topic" + "_publishTopicSingleMsgTtlDeadLetterx", type = "topic"), // 绑定交换机
            key = {DLX_QUEUE_NAME_PREFIX + "kinson.topic.critival"} // 绑定的路由key
    ))
    void consumeTopicSingleMsgTtlDeadLetterx(Message message) throws UnsupportedEncodingException {
        log.info(" ===== consumeTopicSingleMsgTtlDeadLetterx msg is {} ===== ", new String(message.getBody(), "UTF-8"));
    }
}
