package com.rabbitmq.spring.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.rabbitmq.common.constants.CommonConstants.*;


/**
 * @author
 * @Describe topic交换机
 * @date
 */
@Component
@Slf4j(topic = "SpringConsumerTopicDeadLetterx")
public class SpringConsumerTopicDeadLetterx {

    /*****************1.队列设置消息过期ttl交换机、队列等声明*******************/
    @Bean
    public TopicExchange topicExchangeQueueTtl() {
        return new TopicExchange(SPRING_EXCHANGE_NAME_PREFIX + "topic" + "_publishTopicQueueTtlDeadLetterx");
    }

    @Bean
    public Queue queueQueueTtl() {
        // 队列参数
        Map<String, Object> arguments = new HashMap<String, Object>();
        // 为队列设置DLX参数
        arguments.put("x-dead-letter-exchange", DLX_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + "topic" + "_publishTopicQueueTtlDeadLetterx");
        // 为队列设置DLK路由参数x-dead-letter-routing-key
        arguments.put("x-dead-letter-routing-key", DLX_QUEUE_NAME_PREFIX + "kinson.topic.critival");
        // 设置队列中的消息过期时间为10s,超时队列中的消息将会发送到死信交换机上，从而经过路由到死信队列里
        arguments.put("x-message-ttl", 10000);

        // 队列参数设置
        return QueueBuilder.durable(SPRING_QUEUE_NAME_PREFIX + "topic" + "_publishTopicQueueTtlDeadLetterx")
                .withArguments(arguments).build();
    }

    @Bean
    public Binding bindingQueueTtlQueue2TopicExchange(Queue queueQueueTtl, TopicExchange topicExchangeQueueTtl) {
        return BindingBuilder.bind(queueQueueTtl).to(topicExchangeQueueTtl).with("kinson.topic.critival");
    }

    @Bean
    public TopicExchange topicExchangeQueueTtlDeadLetterx() {
        return new TopicExchange(DLX_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + "topic" + "_publishTopicQueueTtlDeadLetterx");
    }

    @Bean
    public Queue queueQueueTtlDeadLetterx() {
        // 队列参数
        Map<String, Object> dlxArguments = new HashMap<String, Object>();
        // 为队列设置DLX参数
        dlxArguments.put("x-queue-mode", "lazy");
        // 设置队列中的消息过期时间为10s
        // arguments.put("x-message-ttl", 10000);

        // 队列参数设置
        return QueueBuilder.durable(DLX_QUEUE_NAME_PREFIX + SPRING_QUEUE_NAME_PREFIX + "topic" + "_publishTopicQueueTtlDeadLetterx")
                .withArguments(dlxArguments).build();
    }

    @Bean
    public Binding bindingQueueTtlDeadLetterxQueue2topicExchange(Queue queueQueueTtlDeadLetterx, TopicExchange topicExchangeQueueTtlDeadLetterx) {
        return BindingBuilder.bind(queueQueueTtlDeadLetterx).to(topicExchangeQueueTtlDeadLetterx).with(DLX_QUEUE_NAME_PREFIX + "kinson.topic.critival");
    }

    /*****************2.单个消息过期设置ttl相关交换机、队列等声明*******************/
    @Bean
    public TopicExchange topicExchangeSingleMsgTtl() {
        return new TopicExchange(SPRING_EXCHANGE_NAME_PREFIX + "topic" + "_publishTopicSingleMsgTtlDeadLetterx");
    }

    @Bean
    public Queue queueSingleMsgTtl() {
        // 队列参数
        Map<String, Object> arguments = new HashMap<String, Object>();
        // 为队列设置DLX参数
        arguments.put("x-dead-letter-exchange", DLX_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + "topic" + "_publishTopicSingleMsgTtlDeadLetterx");
        // 为队列设置DLK路由参数x-dead-letter-routing-key
        arguments.put("x-dead-letter-routing-key", DLX_QUEUE_NAME_PREFIX + "kinson.topic.critival");

        // 队列参数设置
        return QueueBuilder.durable(SPRING_QUEUE_NAME_PREFIX + "topic" + "_publishTopicSingleMsgTtlDeadLetterx")
                .withArguments(arguments).build();
    }

    @Bean
    public Binding bindingSingleMsgTtlQueue2TopicExchange(Queue queueSingleMsgTtl, TopicExchange topicExchangeSingleMsgTtl) {
        return BindingBuilder.bind(queueSingleMsgTtl).to(topicExchangeSingleMsgTtl).with("kinson.topic.critival");
    }

    @Bean
    public TopicExchange topicExchangeSingleMsgTtlDeadLetterx() {
        return new TopicExchange(DLX_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + "topic" + "_publishTopicSingleMsgTtlDeadLetterx");
    }

    @Bean
    public Queue queueSingleMsgTtlDeadLetterx() {
        // 队列参数
        Map<String, Object> dlxArguments = new HashMap<String, Object>();
        // 为队列设置DLX参数
        dlxArguments.put("x-queue-mode", "lazy");
        // 设置队列中的消息过期时间为10s
        // arguments.put("x-message-ttl", 10000);

        // 队列参数设置
        return QueueBuilder.durable(DLX_QUEUE_NAME_PREFIX + SPRING_QUEUE_NAME_PREFIX + "topic" + "_publishTopicSingleMsgTtlDeadLetterx")
                .withArguments(dlxArguments).build();
    }

    @Bean
    public Binding bindingSingleMsgTtlDeadLetterxQueue2TopicExchange(Queue queueSingleMsgTtlDeadLetterx, TopicExchange topicExchangeSingleMsgTtlDeadLetterx) {
        return BindingBuilder.bind(queueSingleMsgTtlDeadLetterx).to(topicExchangeSingleMsgTtlDeadLetterx).with(DLX_QUEUE_NAME_PREFIX + "kinson.topic.critival");
    }

}
