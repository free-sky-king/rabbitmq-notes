package com.rabbitmq.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author
 * @Describe 功能描述
 * @date
 */
@SpringBootApplication
public class RabbitmqSpringStarter {

    public static void main(String[] args) {
        SpringApplication.run(RabbitmqSpringStarter.class, args);
    }
}
