//package com.rabbitmq.spring.api;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.CustomExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import static com.rabbitmq.common.constants.CommonConstants.DELAYED_QUEUE_NAME_PREFIX;
//import static com.rabbitmq.common.constants.CommonConstants.SPRING_EXCHANGE_NAME_PREFIX;
//
///**
// * @author
// * @Describe rabbitmq_delayed_message_exchange插件实现消息延迟，使用前需提前安装好该插件（使用交换机实现延迟效果，时间到达后才发送到对应的queue，进而被queue消费）
// *      rabbitmq延迟插件rabbitmq_delayed_message_exchange：https://www.rabbitmq.com/community-plugins.html
// *      下载插件，然后解压放置到RabbitMQ的插件目录。执行生效命令：rabbitmq-plugins enable rabbitmq_delayed_message_exchange 并重启rabbitmq服务
// * @date
// */
//@Component
//@Slf4j(topic = "SpringConsumerTopicSingleMsgDelayed")
//public class SpringConsumerTopicSingleMsgDelayed {
//    /**
//     * 交换机类型（声明延迟Exchange需要指定type为x-delayed-message）
//     */
//    String exchangeType = "x-delayed-message";
//
//    /*****************1.队列设置消息过期ttl交换机、队列等声明*******************/
//    @Bean
//    public CustomExchange customExchange() {
//        Map<String, Object> arguments = new HashMap<>();
//        // 延迟类型（通过参数x-delay-type指定其Exchange的类型（direct，fanout，topic等））
//        arguments.put("x-delayed-type", "topic");
//        return new CustomExchange(DELAYED_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + exchangeType + "_publishSingleMsgDelayed",
//                exchangeType, true, false, arguments);
//    }
//
//    @Bean
//    public Queue queueDelayed() {
//        return new Queue(DELAYED_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + exchangeType + "_publishSingleMsgDelayed");
//    }
//
//    @Bean
//    public Binding bindingDelayedQueue2CustomExchange(Queue queueDelayed, CustomExchange customExchange) {
//        return BindingBuilder.bind(queueDelayed).to(customExchange).with(DELAYED_QUEUE_NAME_PREFIX + "kinson.topic.critival").noargs();
//    }
//
//}
