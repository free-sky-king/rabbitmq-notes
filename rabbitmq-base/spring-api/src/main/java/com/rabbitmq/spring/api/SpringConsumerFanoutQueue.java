package com.rabbitmq.spring.api;

import com.rabbitmq.client.BuiltinExchangeType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import static com.rabbitmq.common.constants.CommonConstants.SPRING_EXCHANGE_NAME_PREFIX;
import static com.rabbitmq.common.constants.CommonConstants.SPRING_QUEUE_NAME_PREFIX;

/**
 * @author
 * @Describe fanout交换机, 消费端指定队列
 * @date
 */
@Component
@Slf4j(topic = "SpringConsumerFanoutQueue")
public class SpringConsumerFanoutQueue {

    /**
     * 声明队列bean
     *
     * @return
     */
    @Bean
    public Queue fanoutQueue() {
        return new Queue(SPRING_QUEUE_NAME_PREFIX + BuiltinExchangeType.FANOUT.getType() + "_publishFanoutQueue1");
    }

    /**
     * 声明队列bean
     *
     * @return
     */
    @Bean
    public Queue fanoutQueue2() {
        return new Queue(SPRING_QUEUE_NAME_PREFIX + BuiltinExchangeType.FANOUT.getType() + "_publishFanoutQueue2");
    }

    /**
     * 声明队列bean
     *
     * @return
     */
    @Bean
    public Queue fanoutQueue3() {
        return new Queue(SPRING_QUEUE_NAME_PREFIX + BuiltinExchangeType.FANOUT.getType() + "_publishFanoutQueue3");
    }

    /**
     * 声明交换机bean
     *
     * @return
     */
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(SPRING_EXCHANGE_NAME_PREFIX + "fanout" + "_publishFanoutQueue");
    }

    /**
     * 绑定队列
     *
     * @param fanoutQueue
     * @param fanoutExchange
     * @return
     */
    @Bean
    public Binding binding(Queue fanoutQueue, FanoutExchange fanoutExchange) {
        // fanout类型交换机无需绑定路由
        return BindingBuilder.bind(fanoutQueue).to(fanoutExchange);
    }

    /**
     * 绑定队列
     *
     * @param fanoutQueue2
     * @param fanoutExchange
     * @return
     */
    @Bean
    public Binding binding2(Queue fanoutQueue2, FanoutExchange fanoutExchange) {
        // fanout类型交换机无需绑定路由
        return BindingBuilder.bind(fanoutQueue2).to(fanoutExchange);
    }

    /**
     * 绑定队列
     *
     * @param fanoutQueue3
     * @param fanoutExchange
     * @return
     */
    @Bean
    public Binding binding3(Queue fanoutQueue3, FanoutExchange fanoutExchange) {
        // fanout类型交换机无需绑定路由
        return BindingBuilder.bind(fanoutQueue3).to(fanoutExchange);
    }

    /**
     * 监听队列
     *
     * @param msg
     */
    @RabbitListener(queues = SPRING_QUEUE_NAME_PREFIX + "fanout" + "_publishFanoutQueue1")
    void receive(String msg) {
        log.info(" ===== receive msg is {} ===== ", msg);
    }

    /**
     * 监听队列
     *
     * @param msg
     */
    @RabbitListener(queues = SPRING_QUEUE_NAME_PREFIX + "fanout" + "_publishFanoutQueue2")
    void receive2(String msg) {
        log.info(" ===== receive2 msg is {} ===== ", msg);
    }

    /**
     * 监听队列
     *
     * @param msg
     */
    @RabbitListener(queues = SPRING_QUEUE_NAME_PREFIX + "fanout" + "_publishFanoutQueue3")
    void receive3(String msg) {
        log.info(" ===== receive3 msg is {} ===== ", msg);
    }
}
