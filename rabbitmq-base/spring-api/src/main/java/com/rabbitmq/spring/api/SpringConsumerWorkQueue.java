package com.rabbitmq.spring.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static com.rabbitmq.common.constants.CommonConstants.SPRING_QUEUE_NAME_PREFIX;


/**
 * @author
 * @Describe 多个消费者消费同一个队列消息
 * @date
 */
@Component
@Slf4j(topic = "SpringConsumerWorkQueue")
public class SpringConsumerWorkQueue {

    @RabbitListener(queuesToDeclare = @Queue(value = SPRING_QUEUE_NAME_PREFIX + "publishWorkQueue", autoDelete = "true"))
    void receive(String msg) {
        log.info(" ===== receive msg is {} ===== ", msg);
    }

    @RabbitListener(queuesToDeclare = @Queue(value = SPRING_QUEUE_NAME_PREFIX + "publishWorkQueue", autoDelete = "true"))
    void receive2(String msg) {
        log.info(" ===== receive2 msg is {} ===== ", msg);
    }

    @RabbitListener(queuesToDeclare = @Queue(value = SPRING_QUEUE_NAME_PREFIX + "publishWorkQueue", autoDelete = "true"))
    void receive3(String msg) {
        log.info(" ===== receive3 msg is {} ===== ", msg);
    }
}
