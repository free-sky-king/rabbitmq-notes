package com.rabbitmq.spring.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static com.rabbitmq.common.constants.CommonConstants.SPRING_EXCHANGE_NAME_PREFIX;

/**
 * @author
 * @Describe fanout交换机
 * @date
 */
@Component
@Slf4j(topic = "SpringConsumerFanout")
public class SpringConsumerFanout {

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, // 临时队列
            exchange = @Exchange(name = SPRING_EXCHANGE_NAME_PREFIX + "fanout" + "_publishFanout", type = "fanout") // 绑定交换机
    ))
    void receive(String msg) {
        log.info(" ===== receive msg is {} ===== ", msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, // 临时队列
            exchange = @Exchange(name = SPRING_EXCHANGE_NAME_PREFIX + "fanout" + "_publishFanout", type = "fanout")
    ))
    void receive2(String msg) {
        log.info(" ===== receive2 msg is {} ===== ", msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue, // 临时队列
            exchange = @Exchange(name = SPRING_EXCHANGE_NAME_PREFIX + "fanout" + "_publishFanout", type = "fanout")
    ))
    void receive3(String msg) {
        log.info(" ===== receive3 msg is {} ===== ", msg);
    }
}
