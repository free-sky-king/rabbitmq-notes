package com.rabbitmq.spring.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static com.rabbitmq.common.constants.CommonConstants.SPRING_QUEUE_NAME_PREFIX;


/**
 * @author
 * @Describe 当消费端的队列名和发送端路由key相同时，消息能被队列接收并消费
 * @date
 */
@Component
@Slf4j(topic = "SpringConsumerQueue")
@RabbitListener(queuesToDeclare = @Queue(value = SPRING_QUEUE_NAME_PREFIX + "publishQueue", autoDelete = "true"))
public class SpringConsumerQueue {

    @RabbitHandler
    void receive(String msg) {
        log.info(" ===== receive msg is {} ===== ", msg);
    }
}
