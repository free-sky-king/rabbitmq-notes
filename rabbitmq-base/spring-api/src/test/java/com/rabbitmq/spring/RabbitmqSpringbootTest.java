package com.rabbitmq.spring;

import com.rabbitmq.client.BuiltinExchangeType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static com.rabbitmq.common.constants.CommonConstants.*;


/**
 * @author
 * @Describe 功能描述
 * @date
 */
@SpringBootTest(classes = RabbitmqSpringStarter.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class RabbitmqSpringbootTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 当消费端的队列名和发送端路由key相同时，消息能被队列接收并消费
     */
    @Test
    public void publishQueue() {
        for (int i = 1; i <= 3; i++) {
            rabbitTemplate.convertAndSend(SPRING_QUEUE_NAME_PREFIX + "publishQueue", "publishQueue 第" + i + "次发送消息");
        }
    }

    /**
     * 工作队列模式：多个消费者消费同一个队列消息
     */
    @Test
    public void publishWorkQueue() {
        for (int i = 1; i <= 9; i++) {
            rabbitTemplate.convertAndSend(SPRING_QUEUE_NAME_PREFIX + "publishWorkQueue", "publishWorkQueue 第" + i + "次发送消息");
        }
    }

    /**
     * 交换机模式：fanout,该类型无需设置路由key
     */
    @Test
    public void publishFanout() {
        for (int i = 1; i <= 3; i++) {
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.FANOUT.getType() + "_publishFanout",
                    "", "publishFanout 第" + i + "次发送消息");
        }
    }

    /**
     * 交换机模式：direct
     */
    @Test
    public void publishDirect() {
        for (int i = 1; i <= 3; i++) {
            String routingKey = ROUTING_KEYS[(i - 1) % 3];
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.DIRECT.getType() + "_publishDirect",
                    routingKey, "publishDirect 第" + i + "次发送消息, cur routingKey is " + routingKey);
        }
    }

    /**
     * 交换机模式：topic
     */
    @Test
    public void publishTopic() {
        for (int i = 1; i <= 3; i++) {
            String routingKey = TOPIC_ROUTING_KEYS[(i - 1) % 3];
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.TOPIC.getType() + "_publishTopic",
                    routingKey, "publishTopic 第" + i + "次发送消息cur routingKey is " + routingKey);
        }
    }

    /**
     * 交换机模式：headers
     * 消费端需要事先进行队列及交换机声明并进行队列绑定headers交换机及headers属性
     */
    @Test
    public void publishHeaders() {
        // 头部信息
        Map<String, Object> headers = new HashMap<>();
        for (int i = 1; i <= 3; i++) {
            // 消息属性
            MessageProperties messageProperties = new MessageProperties();
            // 设置消息是否持久化。Persistent表示持久化，Non-persistent表示不持久化
            messageProperties.setDeliveryMode(MessageDeliveryMode.NON_PERSISTENT);
            messageProperties.setContentType(UTF_8);
            headers.put("" + i, i);
            messageProperties.getHeaders().putAll(headers);
            Message message = new Message(("publishHeaders 第" + i + "次发送消息").getBytes(), messageProperties);
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.HEADERS.getType() + "_publishHeaders",
                    "", message);
        }
    }

    /**
     * 消息推送确认回调（推送到一个不存在的交换机是否会触发回调方法）
     * 结果：触发ConfirmCallback一个回调函数
     */
    @Test
    public void publishNotExistedExchange() {
        rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + "notExistedExchange",
                "", "publishNotExistedExchange test");
    }

    /**
     * 消息推送确认回调（推送到一个存在的交换机但没有队列绑定，看是否会触发回调方法）
     * 结果：触发ConfirmCallback和RetrunCallback两个回调函数
     */
    @Test
    public void publishExchangeWithoutQueueBind() {
        rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.DIRECT.getType() + "_publishExchangeWithoutQueueBind",
                "publishExchangeWithoutQueueBind", "publishExchangeWithoutQueueBind test");
    }

    /**
     * 消费端指定队列
     */
    @Test
    public void publishDirectQueue() {
        for (int i = 1; i <= 3; i++) {
            String routingKey = ROUTING_KEYS[(i - 1) % 3];
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.DIRECT.getType() + "_publishDirectQueue",
                    routingKey, "publishDirectQueue 第" + i + "次发送消息, cur routingKey is " + routingKey);
        }
    }

    /**
     * 消费端指定队列（手动声明）
     */
    @Test
    public void publishDirectQueueManual() {
        for (int i = 1; i <= 3; i++) {
            String routingKey = ROUTING_KEYS[(i - 1) % 3];
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.DIRECT.getType() + "_publishDirectExchangeManual",
                    routingKey, "publishDirectQueueManual 第" + i + "次发送消息, cur routingKey is " + routingKey);
        }
    }

    /**
     * 消费端指定队列（手动声明）
     */
    @Test
    public void publishFanoutQueueManual() {
        for (int i = 1; i <= 3; i++) {
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.FANOUT.getType() + "_publishFanoutQueue",
                    "", "publishFanoutQueueManual 第" + i + "次发送消息");
        }
    }

    /**
     * 消费端指定队列（手动确认）
     */
    @Test
    public void publishDirectQueueManualAck() {
        for (int i = 1; i <= 3; i++) {
            String routingKey = ROUTING_KEYS[(i - 1) % 3];
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.DIRECT.getType() + "_publishDirectExchangeManualAck",
                    routingKey, "publishDirectQueueManualAck 第" + i + "次发送消息, cur routingKey is " + routingKey);
        }
    }

    /**
     * 消费端指定队列（配置文件全局配置确认方式为手工确认）
     */
    @Test
    public void publishDirectQueueGlobalManualAck() {
        for (int i = 1; i <= 3; i++) {
            String routingKey = ROUTING_KEYS[(i - 1) % 3];
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.DIRECT.getType() + "_publishDirectExchangeGlobalManualAck",
                    routingKey, "publishDirectQueueGlobalManualAck 第" + i + "次发送消息, cur routingKey is " + routingKey);
        }
    }

    /**
     * 死信队列消息(队列设置ttl)
     */
    @Test
    public void publishTopicQueueTtlDeadLetterx() {
        for (int i = 1; i <= 3; i++) {
            String routingKey = TOPIC_ROUTING_KEYS[(i - 1) % 3];
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.TOPIC.getType() + "_publishTopicQueueTtlDeadLetterx",
                    routingKey, "publishTopicQueueTtlDeadLetterx 第" + i + "次发送消息, cur routingKey is " + routingKey);
        }
    }

    /**
     * 死信队列消息(单个消息设置ttl)
     */
    @Test
    public void publishTopicSingleMsgTtlDeadLetterx() {
        for (int i = 1; i <= 3; i++) {
            String routingKey = TOPIC_ROUTING_KEYS[(i - 1) % 3];
            // 发送消息属性设置
            /*MessagePostProcessor messagePostProcessor = new MessagePostProcessor() {
                @Override
                public Message postProcessMessage(Message message) throws AmqpException {
                    // 设置消息过期属性10s
                    message.getMessageProperties().setExpiration("10000");
                    return message;
                }
            };
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.TOPIC.getType() + "_publishTopicSingleMsgTtlDeadLetterx",
                    routingKey, "publishTopicSingleMsgTtlDeadLetterx 第" + i + "次发送消息, cur routingKey is " + routingKey, messagePostProcessor);*/
            // 发送消息并设置消息相关属性
            rabbitTemplate.convertAndSend(SPRING_EXCHANGE_NAME_PREFIX + BuiltinExchangeType.TOPIC.getType() + "_publishTopicSingleMsgTtlDeadLetterx",
                    routingKey, "publishTopicSingleMsgTtlDeadLetterx 第" + i + "次发送消息, cur routingKey is " + routingKey, message -> {
                        // 设置消息过期属性10s
                        message.getMessageProperties().setExpiration("10000");
                        return message;
                    });
        }
    }

    /**
     * 延迟插件rabbitmq_delayed_message_exchange(单个消息设置延迟时间),使用前需提前安装好该插件
     * 对于使用原生的rabbitmq api发送消息的时候需要额外添加header，x-delay，用于设置延迟时间，单位：ms。如下:
     *      int delayMs = 5000;
     *      String msg1 = "delay message " + delayMs;
     *      Map<String, Object> headers = new HashMap<>();
     *      headers.put("x-delay", delayMs);
     *      AMQP.BasicProperties props = new AMQP.BasicProperties.Builder().headers(headers).build();
     *      channel.basicPublish(exchangeName, "", props, msg1.getBytes("utf-8"));
     */
    /*@Test
    public void publishTopicSingleMsgDelayedPluginDelayed() {
        *//**
         * 交换机类型
         *//*
        String exchangeType = "x-delayed-message";
        // 发送消息并设置消息相关属性
        rabbitTemplate.convertAndSend(DELAYED_QUEUE_NAME_PREFIX + SPRING_EXCHANGE_NAME_PREFIX + exchangeType + "_publishSingleMsgDelayed",
                DELAYED_QUEUE_NAME_PREFIX + TOPIC_ROUTING_KEYS[0], "publishSingleMsgDelayed 第" + 1 + "次发送消息", message -> {
                    // 设置消息延迟时间属性10s
                    message.getMessageProperties().setDelay(10000);
                    return message;
                });
    }*/

}
